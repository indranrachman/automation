package stepdefinitions;

import java.awt.event.KeyEvent;
import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import driver.DriverFactory;
import page.Settings;
import page.objects.UtilPageObject;
import page.utils.Captures;

public class Step {
	public WebDriver driver;
	Actions action;
	
	
	UtilPageObject x = new UtilPageObject();

	public Step() {
		driver = DriverFactory.getWebDriver();
	}

	
	String Type;
	
	
	@Given("^User Git Login$")
	public void user_git_login() throws Throwable {
		UtilPageObject.LoginGit(driver, Settings.getusernameGit(), Settings.getpasswordGit());
		System.out.println(strPath);
	
	}
	
		@When("^User do create Gists and input data$")
	public void user_do_create_gists(String yourDesc, String fileName, String codeHere) throws Exception {
		
		driver.findElement(By.xpath("//summary[@class='HeaderNavlink name mt-1']//img[@class='avatar float-left mr-1']")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Your gists')]")).click();
		
		//Input data
		driver.findElement(By.xpath("//input[@placeholder='Gist description']")).sendKeys(yourDesc);
		driver.findElement(By.xpath("//input[@placeholder='Filename including extension']")).sendKeys(fileName);
		driver.findElement(By.xpath("//div[@class='CodeMirror-lines']")).sendKeys(codeHere);
		do {
			driver.findElement(By.xpath("//button[@value='1']")).click();
		} while (!UtilPageObject.isButtonExist(driver, "//button[@value='0']"));
	
		new WebDriverWait(driver, 120).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(text(),'Raw')]")));
	}
	
	
	@Then("^I should see my new gists$")
	public void i_should_see_my_new_gist() throws Throwable {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//summary[@class='HeaderNavlink name mt-1']//img[@class='avatar float-left mr-1']")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Your gists')]")).click();
		
		Captures.takeSimpleFullScreenShot();
		
		
	}
	
	@When("^User do edit Gists and input data$")
	public void user_do_edit_gists(String editComment, String fileName) throws Exception {
		
		driver.findElement(By.xpath("//summary[@class='HeaderNavlink name mt-1']//img[@class='avatar float-left mr-1']")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Your gists')]")).click();
		
		driver.findElement(By.xpath("//strong[@class='css-truncate-target'][contains(text(),'Testlagideh.java')]")).click();
		driver.findElement(By.xpath("//*[@class='octicon octicon-pencil']")).click();
		
		driver.findElement(By.xpath("//textarea[@placeholder='Leave a comment']")).sendKeys(editComment);
		
		driver.findElement(By.xpath("//input[@placeholder='Filename including extension']")).sendKeys(fileName);
		
		do {
			driver.findElement(By.xpath("//button[contains(text(),'Comment')]")).click();
		} while (!UtilPageObject.isButtonExist(driver, "//a[@data-ga-click='Footer, go to terms, text:terms']"));
	
			}
	
	@Then("^I should see my edited gists$")
	public void i_should_see_my_edited_gist() throws Throwable {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//summary[@class='HeaderNavlink name mt-1']//img[@class='avatar float-left mr-1']")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Your gists')]")).click();
		
		Captures.takeSimpleFullScreenShot();
		
		
	}
	
	@When("^User do delete Gists and input data$")
	public void user_do_delete_gists(String editComment, String fileName) throws Exception {
		
		driver.findElement(By.xpath("//summary[@class='HeaderNavlink name mt-1']//img[@class='avatar float-left mr-1']")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Your gists')]")).click();
		
		String fileToDelete = driver.findElement(By.xpath("/html[1]/body[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/span[2]")).getAttribute("value").toString();
		
		driver.findElement(By.xpath("/html[1]/body[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/span[1]/a[2]/strong[1]")).click();
		driver.findElement(By.xpath("//input[@value='delete']")).click();
		
		do {
			if (UtilPageObject.isAlertPresents(driver)) {
				driver.switchTo().alert().accept();
				UtilPageObject.Scroll_Up(driver);
				UtilPageObject.Scroll_Up(driver);
			}
		} while (UtilPageObject.isAlertPresents(driver));
		Thread.sleep(1000);
		
		String fileFirst = driver.findElement(By.xpath("/html[1]/body[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/span[2]")).getAttribute("value").toString();
		
		if (fileToDelete.equals(fileFirst))
		{
			System.out.println("Failed to Delete");
		}

	}
	
	@Then("^My git successfully deleted$")
	public void my_git_succeed_delete() throws Throwable {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//summary[@class='HeaderNavlink name mt-1']//img[@class='avatar float-left mr-1']")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Your gists')]")).click();
		
		Captures.takeSimpleFullScreenShot();
		
	}
	
	
	@When("^User do check list Gists$")
	public void user_check_list(String editComment, String fileName) throws Exception {
		
		driver.findElement(By.xpath("//summary[@class='HeaderNavlink name mt-1']//img[@class='avatar float-left mr-1']")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Your gists')]")).click();
		
		
	}
	
	@Then("^i can see list$")
	public void i_can_see_list() throws Throwable {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//summary[@class='HeaderNavlink name mt-1']//img[@class='avatar float-left mr-1']")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Your gists')]")).click();
		
		Captures.takeSimpleFullScreenShot();
		
	}
	
	




}
