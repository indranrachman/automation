package page.objects;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.crypto.spec.SecretKeySpec;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.List;
import driver.DriverFactory;
import page.DecryptPass;
import page.Settings;
import page.utils.Captures;

public class UtilPageObject {
	private static SecretKeySpec secretKey;
	private static byte[] key;

	/**
	 * @throws Throwable will be thrown if an error pop up shown
	 */
	public static void waitLoadingToBeDone() throws Throwable {
		try {
			Thread.sleep(1000);
			// Date started = new Date();
			for (int i = 0; i < 60; i++) {
				if (ifUsernameClickable())
					break;

				ifErrorPopUpAppear();
				Thread.sleep(1000);
			}
			// System.out.println("Loading takes : " + (new Date().getSeconds()
			// - started.getSeconds()));
		} catch (Exception e) {

		}
	}

	/**
	 * @return true if the page is blocked by some pop up
	 */
	
	
	
	public static void setKey(String myKey) {
		MessageDigest sha = null;
		try {
			key = myKey.getBytes("UTF-8");
			sha = MessageDigest.getInstance("SHA-1");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 16);
			secretKey = new SecretKeySpec(key, "AES");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	

	public static boolean isAlertPresents(WebDriver driver) {

		try {
			Thread.sleep(120);
			driver.switchTo().alert();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isButtonExist(WebDriver driver, String Xpath) {
		try {
			Thread.sleep(120);
			driver.findElement(By.xpath(Xpath)).isEnabled();
			return true;
		} catch (Exception e) {

			return false;
		}
	}

	public static boolean Scroll_Up(WebDriver driver) throws InterruptedException {
		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,-250)", "");
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static void Scroll_Up_Robot() throws InterruptedException {
		try {
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_PAGE_UP);
			robot.keyRelease(KeyEvent.VK_PAGE_UP);
			robot.keyPress(KeyEvent.VK_PAGE_UP);
			robot.keyRelease(KeyEvent.VK_PAGE_UP);
			robot.keyPress(KeyEvent.VK_PAGE_UP);
			robot.keyRelease(KeyEvent.VK_PAGE_UP);
		} catch (Exception e) {
			return;
		}
	}

	public static boolean Scroll_Right(WebDriver driver) throws InterruptedException {
		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(2000,0)", "");
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static void Scroll_Down_Robot() throws InterruptedException {
		try {
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_PAGE_DOWN);
			robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
		} catch (Exception e) {
			return;
		}
	}

	
	public static void LoginGit(WebDriver driver, String UserName, String Password) throws Throwable {
		driver.findElement(By.xpath("//input[@id='login_field']")).sendKeys(UserName);
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys(Password);
		driver.findElement(By.xpath("//input[@value='Sign in']")).click();
		new WebDriverWait(driver, 200).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='octicon octicon-mark-github']")));
	}
	

}
