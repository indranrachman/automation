package runner.parallel;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		strict = true, 
		//tags = "@maturity", 
		dryRun = false, 
		features = "@target/Retry", 
		plugin = {
		"pretty", "json:target/cucumber/report_Retry.json",
		"html:target/cucumber/report_Retry.html" }, monochrome = true,
		glue = { "stepdefinitions" })
public class Retry {
}