package runner.parallel;                                                                                                                                                                                                                                                                      

import org.junit.runner.RunWith;
 
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		strict = true, 
		tags = "@Excute1",
		dryRun = false, 
				features = "src/test/resources/features/Gists.feature", 
						plugin = {
		"pretty", "json:target/cucumber/json/report_debug06.json",
		"html:target/cucumber/report_debug06.html","rerun:target/Life/rerun_Life01.txt" }, monochrome = true,
		glue = { "stepdefinitions" })
public class Executor {
}